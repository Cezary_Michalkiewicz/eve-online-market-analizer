# Eve Online Market Analizer

Tool for mmo-rpg game Eve Online created in Python.
Analyzes market of chosen item and helps players quickly compare price in different regions. 

website used: https://evemarketer.com/


You will get min and max price in global market, as well as profit from selling chosen item in diffirent regions, compared to main market hub, Jita IV.

You type in name of desired item in enterbox, dynamic filtering gives you a list of matching items, you double click on this item to get case wright and google chrome starts running in the background. After couple of secons you have your item with data of orders parsed from a webpage.
You can now export just basic hints or all orders to excel.


Packages used:
selenium, pandas, BeautifulSoup and simple GUI: guizero.
