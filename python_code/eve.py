import pandas as pd
from bs4 import BeautifulSoup as beautysoup
from selenium import webdriver
import datetime
from time import sleep
import guizero
import re


class Main:

    def __init__(self):
        pd.set_option('precision', 2)
        pd.options.display.float_format = '{:.2f}'.format

        chromeOptions = webdriver.ChromeOptions()
        prefs = {'profile.managed_default_content_settings.images': 2, 'disk-cache-size': 4096}
        chromeOptions.add_experimental_option("prefs", prefs)

        self.driver = webdriver.Chrome(chrome_options=chromeOptions)
        self.url = 'https://evemarketer.com/types/'


        self.get_dict()
        self.info = []
        self.df = None

    def get_dict(self):
        self.items_dict = {}
        with open('eve_online_items.txt') as f:
            for line in f:
                value, key = line.split('|')
                key = key.replace('\n', '')
                key = key.replace("'", '')
                self.items_dict[key] = value
            f.close()

        return self.items_dict

    def get_item(self, item):
        self.driver.get(self.url + item)

    def scrap_table(self):
        sleep(2)
        table = self.driver.find_element_by_class_name('table')
        table_html = table.get_attribute('innerHTML')
        soup = beautysoup(table_html, 'lxml')
        rows = soup.find_all('tr')
        headers = ['REGION', 'QUANTITY', 'PRICE', 'LOCATION', 'EXPIRES IN', 'RECEIVED AT']
        df = pd.DataFrame(columns=headers)

        for row in rows[1:]:
            row.tds = row.find_all('td')
            record = []
            for td in row.tds:
                record.append(td.text.replace('\n', ''))
            df.loc[len(df)] = record
        self.df = df

    def replacements(self, df):
        # replace wrong characters
        df.LOCATION = df.LOCATION.str.replace('\n', '')
        locations = [re.sub("\s\s+", " ", x) for x in df.LOCATION.tolist()]
        df.LOCATION = locations

        df.PRICE = df.PRICE.str.replace('ISK', '')
        df.PRICE = df.PRICE.str.replace(',', '')
        df.QUANTITY = df.QUANTITY.str.replace(',', '')

        # change data types
        df.PRICE = df.PRICE.astype('float32')
        df.PRICE = df.PRICE.round()
        df.QUANTITY = df.QUANTITY.astype('int64')

    def summarize(self, df):
        self.grouped_table = df[['REGION', 'LOCATION','PRICE']].groupby(['REGION','LOCATION'], as_index=False).min()
        self.grouped_table.style.format({'PRICE': '{:,}'})

    def get_hints(self, df):
        # display info about market in different regios
        jita = '0.9 Jita IV - Moon 4 - Caldari Navy Assembly Plant '
        names = ['The Forge', 'Metropolis', 'Domain', 'Sinq Laison']
        regioned_df = df.groupby(['REGION']).min()
        regioned_df.style.format({'PRICE': '{:,}'})

        # getting min, max value
        min_price = regioned_df.PRICE.min()
        max_price = regioned_df.PRICE.max()

        min_regions = regioned_df.index[regioned_df.PRICE == min_price].tolist()
        max_regions = regioned_df.index[regioned_df.PRICE == max_price].tolist()

        # creating info
        min_info = 'Minimum price is {} in {}.'.format('{:,}'.format(min_price), ', '.join(min_regions))
        max_info = 'Maximum price is {} in {}.'.format('{:,}'.format(max_price), ', '.join(max_regions))
        compare_info = '\nComparing to Jita:\n'

        places = df.LOCATION.tolist()
        if jita in places:
            jita_val = list(df['PRICE'].loc[df.LOCATION == jita])[0]
            jita_info = '\nJita sells this for {}'.format('{:,}'.format(jita_val))

        else:
            pass
            jita_val = 0
            jita_info = '\nThere are no orders in Jita.'

        # container to hold information
        self.info = ['\n', min_info, max_info, jita_info]

        if jita_val != 0:
            self.info.append(compare_info)
        self.info.append('\n')

        # info about prices in four main regions
        for name in names:  # except The Forge
            if name in regioned_df.index:
                region_val = regioned_df.at[name, 'PRICE']
                if jita_val != 0:
                    profit = '{:,}'.format(region_val - jita_val)
                    self.info.append('You can make {} selling this in {} for a {}'.format(profit, name, '{:,}'.format(region_val)))
                else:
                    self.info.append('{} sells this for {}.'.format(name, '{:,}'.format(region_val)))
            else:
                self.info.append('No one is selling in {}.'.format(name))

    def search_web(self, item):
        # all methods used to run webbrowser
        self.get_item(item)
        self.scrap_table()
        self.replacements(self.df)
        self.summarize(self.df)
        self.get_hints(self.grouped_table)


class Gui(Main):
    def __init__(self):
        super().__init__()
        Main.get_dict(self)

        # app - main window
        self.app = guizero.App(title='Input your item', layout='grid', height=800, width=1250)
        self.app.bg = 'black'
        self.app.font = 'Open Sans'
        self.app.text_size = 12
        self.text_color = 'white'

        # enterbox for items
        self.enterbox = guizero.TextBox(self.app, grid=[0, 0, 2, 1], width=40)  # enterbox
        self.enterbox.bg = 'white'
        self.enterbox.text_color = 'black'
        self.enterbox.when_key_released = self.filter_items
        self.enterbox.text = ''

        # runs browser
        self.button = guizero.PushButton(self.app, text='Search', command=self.assign_item, grid=[0, 1, 2, 1])  # button
        self.button.text_color = 'white'
        self.button.width = 10

        # items listbox
        self.listbox = guizero.ListBox(self.app, sorted(self.items_dict), grid=[0, 3])  # listobx
        self.listbox.width = 50
        self.listbox.height = 35
        self.listbox.text_color = 'white'
        self.listbox.when_left_button_pressed = self.take_value_to_enterbox

        # contains summary of current market
        self.display_box = guizero.ListBox(self.app, self.info, grid=[3, 3])
        self.display_box.width = 70
        self.display_box.height = 35
        self.display_box.bg = 'white'

        # export buttons
        # this button exports all orders of an item to excel
        self.button_export_all = guizero.PushButton(self.app, text='Export all orders', grid=[2, 1], command=self.export_all)
        self.button_export_all.width = 10
        self.button_export_all.text_color = 'white'

        # this button exports only summary to excel
        self.button_export_hints = guizero.PushButton(self.app, text='Export hints only', grid=[2, 2], command=self.export_hints)
        self.button_export_hints.width = 10
        self.button_export_hints.text_color = 'white'

        self.app.display()

    def take_value_to_enterbox(self):
        self.enterbox.value = self.listbox.value

    def filter_items(self):
        # auto-filters items displayed in listbox based off current characters
        self.listbox.clear()

        for element in sorted(self.items_dict):
            text = self.enterbox.value  # current characters in enterbox
            if text.lower() == element[:len(text)].lower():
                self.listbox.append(element)

    def assign_item(self):
        self.my_item = self.items_dict[self.enterbox.value]
        self.my_item_key = self.enterbox.value
        Main.search_web(self, self.my_item)

        self.display_box.clear()
        self.display_box.append(self.enterbox.value)
        for elem in self.info:
            self.display_box.append(elem)

    # methods used when exporting file
    # window to input path

    def choose_path(self):
        self.window = guizero.Window(self.app, title='Choose your path')

        self.window.bg = self.app.bg
        self.window.font = self.app.font
        self.window.text_size = self.app.text_size
        self.window.text_color = 'white'

        self.path_enterbox = guizero.TextBox(self.window, text=r'D:\{}_{}.xlsx'.format(self.my_item_key, datetime.datetime.now().date()),
                                             width=40)
        self.path_enterbox.text_color = 'white'

        self.save_button = guizero.PushButton(self.window, text='Save', command=self.save_file)
        self.save_button.width = 10
        self.save_button.text_color = 'white'

        self.cancel_button = guizero.PushButton(self.window, text='Cancel', command=self.quit_window)
        self.cancel_button.text_color = 'white'
        self.cancel_button.width = 10

    def quit_window(self):
        self.window.hide()

    def export_all(self):
        self.my_method = self.export_all
        self.choose_path()

    def export_hints(self):
        self.my_method = self.export_hints
        self.choose_path()

    def save_file(self):
        path = self.path_enterbox.value
        writer = pd.ExcelWriter(path)
        self.df.to_excel(writer, sheet_name=self.my_item)
        writer.save()
        self.window.hide()


if __name__ == '__main__':
    a = Gui()
